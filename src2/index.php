<?php

require 'RegisterUser.php';
require 'AuthController.php';

$registration = new RegisterUser;
$authController = new AuthController($registration);

$authController->register();
