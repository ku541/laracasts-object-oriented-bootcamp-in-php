<?php

interface CanBeFiltered
{
    public function filter($data);
}

class Favorited implements CanBeFiltered
{
    public function filter($data)
    {
        var_dump('Log the message to a file:' . $data);
    }
}

class Uwatched implements CanBeFiltered
{
    public function filter($data)
    {
        var_dump('Log the message to a DB:' . $data);
    }
}

class Difficulty implements CanBeFiltered
{
    public function filter($data)
    {
        var_dump('Log the message to a DB:' . $data);
    }
}

class UserController
{
    protected $logger;

    public function __construct(Logger $logger)
    {
        $this->logger = $logger;
    }

    public function show()
    {
        $user = 'John Doe';

        $this->logger->execute($user);
    }
}

$controller = new UserController(new LogToDB);
$controller->show();
